import React, { Component } from 'react';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import 'antd/dist/antd.css';
import './register.scss';
import axios from 'axios';
import { Modal , Select } from 'antd';
const { Option, OptGroup } = Select;

class Register extends Component {
    constructor() {
        super();
    
        this.state = {
          npiNumber : undefined,
          emailAddress : undefined,
          phoneNumber : undefined,
          businessAddress : undefined,
          firstName: undefined,
          lastName: undefined,
          userId : undefined,
          showSucess:false,
          visible: false,
          isValid: true,
          errorMessage:'',
          successMessage:'',
          successStatusCode:''
        };
    
        this.onNpiNumberChange = this.onNpiNumberChange.bind(this);
        this.onEmailAddressChange = this.onEmailAddressChange.bind(this);
        this.onPhoneNumberChange = this.onPhoneNumberChange.bind(this);
        this.onFirstNameChange = this.onFirstNameChange.bind(this);
        this.onLastNameChange = this.onLastNameChange.bind(this);
        this.onBusinessAddressChange = this.onBusinessAddressChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    
      }
    
      onNpiNumberChange = (e) => {
        this.setState({
          npiNumber : e.target.value
        });
      }

      onEmailAddressChange = (e) => {
        this.setState({
           emailAddress : e.target.value
        });
      }

      onPhoneNumberChange = (e) => {
        this.setState({
          phoneNumber : e.target.value
       });
      }

      onBusinessAddressChange = (e) => {
        this.setState({
           businessAddress : e.target.value
        });
      }

      onFirstNameChange = (e) =>{
        this.setState({
          firstName : e.target.value
       });
      }

      handleCancel = e => {
        console.log(e);
        this.setState({
          visible: false,
        });
      };

      handleOk = e => {
        console.log(e);
        this.setState({
          visible: false,
        });
        

       const userObject = {
        first_name: this.state.firstName,
        last_name:this.state.lastName,
        npi_number: this.state.npiNumber,
        email_address:this.state.emailAddress,
        business_address:this.state.businessAddress,
        phone_number: this.state.phoneNumber
       };

      axios.post('http://localhost:8090/api/printDataPoints', userObject)
         .then((res) => {
              console.log(res.data);
            this.setState({ showSucess:true , successMessage : res.data.message, successStatusCode:res.data.statusCode});
            
          }).catch((error) => {
               console.log(error)
               this.setState({ errorMessage: 'Sorry, unable to save data '});
          });
      };

      onUserIdChange = (e) => {
        this.setState({
          userId : e.target.value
       });
      }

      onLastNameChange = (e) => {
        this.setState({
          lastName : e.target.value
        });
      }
    
      onSubmit = (e) => {
        e.preventDefault();
        console.log(this.state);
        this.setState({
          visible: true,
          errorMessage: undefined
        });     
      }
    
      render() {
        const { firstName, lastName, npiNumber, businessAddress, phoneNumber , emailAddress,showSucess , successMessage,successStatusCode} = this.state;
        if(!showSucess)
        return (
          <section id="cover" class="min-vh-100">
          <div id="cover-caption">
          <div className="container">
          <div className="row" style={{marginTop: '10px'}}> 
 
          <div className="col-xl-5 col-lg-6 col-md-8 col-sm-10 mx-auto text-center form p-4">
                  <h1 className="display-7 py-2">Registration Form</h1>
              { this.state.errorMessage &&
                 <h6 className="error" style={{ color: 'red' }}> { this.state.errorMessage } </h6> }

                  <div className="px-2">
                      <form onSubmit={this.onSubmit} autoComplete='off' className="justify-content-center">

                      <div className="form-group">
                              <label className="sr-only">NPI number</label>
                              <input type="text" className="form-control" maxLength={10} minLength={10} value={npiNumber}  onChange={this.onNpiNumberChange} required placeholder="NPI number"/>

                        </div>       
                      <div className="form-group">
                              <label className="sr-only">First Name</label>
                              <input type="text" className="form-control" value={firstName}  onChange={this.onFirstNameChange} required placeholder="First Name"/>
                        </div>
                          <div className="form-group">
                              <label className="sr-only">Last Name</label>
                              <input type="text" className="form-control"  onChange={this.onLastNameChange} value={lastName}  required placeholder="Last Name"/>
                          </div>

                          <div className="form-group">
                              <label className="sr-only">Email Address</label>
                              <input type="text" className="form-control"  onChange={this.onEmailAddressChange} value={emailAddress}  required placeholder="Email Address"/>
                          </div>

                           <div className="form-group">
                              <label className="sr-only">Phone number</label>
                              <input type="text" className="form-control"  onChange={this.onPhoneNumberChange} value={phoneNumber}  required placeholder="Phone Number"/>
                          </div>

                           <div className="form-group">
                              <label className="sr-only">Business Address</label>
                              <input type="text" className="form-control"  onChange={this.onBusinessAddressChange} value={businessAddress}  required placeholder="Business Address"/>
                          </div>

                          <button type="submit" id="btn-debc-primary" className="btn btn-primary btn-lg">Submit</button>

                      </form>
                      <Modal
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <p>Please make sure your information is correct before you submit the form?</p>

        </Modal>
                  </div>
            
              </div>
          </div>
      </div>
</div>
</section>
       );

       if(showSucess && successStatusCode=== '200')
       return (
        <section id="cover" class="min-vh-100">
        <div id="cover-caption">
        <div className="container">
        <div className="row text-white">
        </div>
        <div className="text-center form p-4">
          
       <h1 className="display-8 py-2" style={{  color: 'white'}}>{successMessage}</h1>
          </div>        
          </div>
        </div>
          </section>

       );

       if(showSucess && successStatusCode === '201')
       return (
        <section id="cover" class="min-vh-100">
        <div id="cover-caption">
        <div className="container">
        <div className="row text-white">
        </div>
        <div className="text-center form p-4">
          
       <h1 className="display-8 py-2" style={{  color: 'red'}}>Not sucessful</h1>
      </div>        
          </div>
        </div>
          </section>

       );
      }
    }    
    
  export default Register;